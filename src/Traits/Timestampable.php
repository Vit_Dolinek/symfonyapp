<?php


namespace App\Traits;

use Doctrine\ORM\Mapping as ORM;

trait Timestampable
{
    /**
     * @ORM(type="datetime")
     */
    private $created;

    /**
     * @ORM(type="datetime", nullable=true)
     */
    private $updated;

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @ORM\PrePersist()
     */
    public function onEntityCreated()
    {
        $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onEntityUpdated()
    {
        $this->setUpdated(new \DateTime());
    }

}