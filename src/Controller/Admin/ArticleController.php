<?php


namespace App\Controller\Admin;

use App\Entity\Article;
use App\Exception\UserException;
use App\Repository\ArticleRepository;
use App\Service\ArticleManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package App\Controller\Admin
 * @Route("/admin/articles")
 */
class ArticleController extends BaseAdminController
{
    /**
     * @Route("", name="admin_articles_index")
     *
     * @param ArticleRepository $articleRepository
     *
     * @return Response
     */
    public function index(ArticleRepository $articleRepository): Response
    {
        return $this->render('admin/article/index.html.twig', [
            $articles = $articleRepository->findAll()
        ]);
    }

    /**
     * @Route("/add", name="admin_articles_add")
     *
     * @param Request $request
     * @param ArticleManager $articleManager
     *
     * @return Response
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Request $request, ArticleManager $articleManager): Response
    {
        $post = $request->request;
        if ($post->get('save')) {
            $errors = 0;
            if (!$this->isCsrfTokenValid('save', $post->get('csrf'))) {
                $this->addFlash('error', 'Invalid data submitted');
                $errors++;
            }

            if (!$post->get('title') || !$post->get('content')) {
                $this->addFlash('error', 'Both title and content are mandatory');
                $errors++;
            }

            if (!$errors) {
                try {
                    $article = $articleManager->createArticle(
                        $post->get('title'),
                        $post->get('content'),
                        $post->get('categories')
                    );

                    return $this->redirectToRoute('admin_articles_edit', [
                        'id' => $article->getId()
                    ]);
                } catch (UserException $e) {
                    $this->addFlash('error', 'Could not create article due to internal error.');
                }
            }
        }

        return $this->render('admin/article/add.html.twig');
    }

    /**
     * @Route("/edit/{id}", name="admin_articles_edit")
     *
     * @param Article $article
     *
     * @return Response
     */
    public function edit(Article $article): Response
    {
        return $this->render('admin/articles/edit.html.twig');
    }

    /**
     * @Route("/delete/{id}", name="admin_articles_delete")
     *
     * @param Article                $article
     * @param EntityManagerInterface $entityManager
     *
     * @return RedirectResponse
     */
    public function delete(Article $article, EntityManagerInterface $entityManager): RedirectResponse
    {
        return $this->redirectToRoute('admin_articles_index');
    }
}
