<?php


namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller\Admin
 * @Route("/admin")
 */
class AdminController extends BaseAdminController
{
    /**
     * @Route("", name="admin_index")
     * @return Response
     */
    public function index()
    {
        return $this->render('admin/index.html.twig');
    }
}