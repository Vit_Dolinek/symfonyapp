<?php


namespace App\Controller\Application;


use App\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomepageController
 * @package App\Controller\Application
 * @Route("/")
 */
class HomepageController extends BaseController
{
    /**
     * @Route("", name="index")
     */
    public function index()
    {
        return $this->render('application/index.html.twig');
    }
}