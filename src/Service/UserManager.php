<?php


namespace App\Service;


use App\Entity\User;
use App\Exception\UserException;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManager $entityManager,
        TokenStorageInterface $tokenStorage
    ) {

        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     *
     * @return User
     *
     * @throws UserException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createUser(string $username, string $email, string $password): User
    {
        if ($this->userExists($username)) {
            throw new UserException('The username is already taken.');
        }

        if ($this->emailExists($email))

        $user = new User();
        $user
            ->setUsername($username)
            ->setEmail($email)
            ->setPassword($this->passwordEncoder->encodePassword($user, $password));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function userExists(string $username): bool
    {
        return $this->userRepository->findOneBy([
            'username' => $username
        ]) !== null;
    }

    public function emailExists(string $email): bool
    {
        return $this->userRepository->findOneBy([
            'email' => $email
        ]) !== null;
    }

    public function getUser(string $username): User
    {
        if (!$this->userExists($username)) {
            throw new UserException("The user with username '{$username}' does not exist.");
        }

        return $this->userRepository->findOneBy([
            'username' => $username
        ]);
    }

    /**
     * @return User
     *
     * @throws UserException
     */
    public function getCurrentUser(): User
    {
        $exception = new UserException('No user is currently logged in.');
        $token = $this->tokenStorage->getToken();

        if (is_null($token)) {
            throw $exception;
        }

        $user = $token->getUser();

        if (!$user instanceof User) {
            throw $exception;
        }

        return $user;
    }

}