<?php


namespace App\Service;


interface SlugCreatorInterface
{
    /**
     * Creates slug suitable for url
     *
     * @param string $originalString
     *
     * @return string
     */
    public function generate(string $originalString): string;
}