<?php


namespace App\Service;


use App\Entity\Article;
use App\Exception\UserException;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class ArticleManager
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var SlugCreatorInterface
     */
    private $slugCreator;

    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(
        ArticleRepository $articleRepository,
        EntityManager $entityManager,
        SlugCreatorInterface $slugCreator,
        UserManager $userManager
    ) {
        $this->articleRepository = $articleRepository;
        $this->entityManager = $entityManager;
        $this->slugCreator = $slugCreator;
        $this->userManager = $userManager;
    }

    /**
     * @param string $title
     * @param string $content
     * @param array $categories
     *
     * @return Article
     *
     * @throws UserException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createArticle(string $title, string $content, array $categories): Article
    {
        $article = new Article();
        $article
            ->setTitle($title)
            ->setContent($content)
            ->setAuthor($this->userManager->getCurrentUser())
            ->setSlug($this->slugCreator->generate($title));

        foreach ($categories as $category) {
            $article->setCategory($category);
        }

        $this->entityManager->persist($article);
        $this->entityManager->flush();

        return $article;

    }

}