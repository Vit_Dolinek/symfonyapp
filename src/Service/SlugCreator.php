<?php


namespace App\Service;


use App\Repository\ArticleRepository;

class SlugCreator implements SlugCreatorInterface
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }


    /**
     * Creates slug suitable for url
     *
     * @param string $originalString
     *
     * @return string
     */
    public function generate(string $originalString): string
    {
        $slug = preg_replace('@\s+@', '-', $originalString);
        $slug = preg_replace('@[^a-zA-Z0-9]@', '', $slug);
        $slug = preg_replace_callback('@([A-Z])@', function ($matches) {
            return strtolower($matches[1]);
        }, $slug);

        return $this->getUnique($slug);
    }

    private function getUnique(string $slug, int $i = 0): string
    {
        $newSlug = $slug;
        if ($i > 0) {
            $newSlug .= "-{$i}";
        }
        if ($this->articleRepository->findOneBy([
            'slug' => $newSlug,
        ])) {
            return $this->getUnique($slug, $i + 1);
        }

        return $newSlug;
    }
}